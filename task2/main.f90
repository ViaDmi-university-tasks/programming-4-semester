program main
    use :: Task
    use :: mpi
    implicit none
    integer :: n, row
    integer :: x1, x2, y1, y2
    integer(4) :: mpiErr, mpiSize, mpiRank
    real(8) :: startTime, endTime
    real(8), allocatable :: A(:,:)
    
    allocate(A(1000,1000))
    call random_number(A)
    A = A - 0.5d0
    
    call mpi_init(mpiErr)
    
    call mpi_comm_size(mpi_comm_world, mpiSize, mpiErr)
    call mpi_comm_rank(mpi_comm_world, mpiRank, mpiErr)
    startTime = MPI_WTIME()
    call GetMaxCoordinates(A, x1, y1, x2, y2)
    endTime = MPI_WTIME()
    
    if(mpiRank == 0) then
        write(*,*) 'Coordinates of maximum submatrix:'
        write(*,*) x1, y1
        write(*,*) x2, y2
        write(*,*) 'Time: ', endTime - startTime
    endif

    call mpi_finalize(mpiErr)  
end program
