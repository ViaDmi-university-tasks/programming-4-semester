program main
    use :: omp_lib
    use :: Task
    implicit none
    integer :: n, i
    integer :: x1, x2, y1, y2
    integer :: numbers(1:4)
    real(8) :: startTime, endTime
    real(8), allocatable :: A(:,:)
    
    allocate(A(2000,2000))
    call random_number(A)
    A = A - 0.5d0
    numbers = (/1, 2, 4, 50/)
    
    do i = 1, 4
      call omp_set_num_threads(numbers(i))
      startTime = omp_get_wtime()
      call GetMaxCoordinates(A, x1, y1, x2, y2)
      endTime = omp_get_wtime()
      write(6,*) 'Numbers of therads:', numbers(i)
      write(6,*) 'Coordinates of maximum submatrix:'
      write(6,*) x1, y1, '#', x2, y2
      write(6,*) 'Time: ', endTime - startTime
    enddo
    
end program
