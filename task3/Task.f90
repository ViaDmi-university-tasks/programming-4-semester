module Task
use :: fgsl
use, intrinsic :: iso_c_binding
implicit none
  type splineData
    type(fgsl_spline) :: spline
    type(fgsl_interp_accel) :: acceleration
  end type
contains

  function GetSplineValue(x, x_array, y_array)
    real(fgsl_double), dimension(:), intent(in) :: x_array, y_array
    real(fgsl_double), intent(in) :: x
    real(fgsl_double) :: GetSplineValue
    integer(fgsl_size_t) :: array_size
    integer(fgsl_int) :: spline_status
    type(fgsl_interp_accel) :: lookup_acceleration
    type(fgsl_spline) :: spline

    array_size = size(x_array)
    spline = fgsl_spline_alloc(fgsl_interp_cspline, array_size)
    spline_status = fgsl_spline_init(spline, x_array, y_array)
    GetSplineValue = fgsl_spline_eval(spline, x, lookup_acceleration)
    call fgsl_spline_free(spline)
    call fgsl_interp_accel_free(lookup_acceleration)
  end function GetSplineValue
  
  function integrable_c_function(x, spline_c_ptr) bind(c)
    real(c_double), value :: x
    type(c_ptr), value :: spline_c_ptr
    real(c_double) :: integrable_c_function
    type(splineData), pointer :: spline_f_ptr
    call c_f_pointer(spline_c_ptr, spline_f_ptr)
    integrable_c_function = fgsl_spline_eval(spline_f_ptr%spline, x, spline_f_ptr%acceleration)
  end function integrable_c_function
  
  function GetSplineIntegral(a, b, x_array, y_array)
    real(fgsl_double), dimension(:), intent(in) :: x_array, y_array
    real(fgsl_double), intent(in) :: a, b
    real(fgsl_double) :: GetSplineIntegral
    integer(fgsl_int) :: integration_status, spline_status
    integer(fgsl_size_t) :: array_size
    integer(fgsl_size_t), parameter :: max_number_intervals = 1000
    real(fgsl_double) :: integration_error, absolute_error, relative_error
    type(splineData), target :: spline_data
    type(c_ptr) :: spline_c_ptr
    type(fgsl_function) :: integrable_f_function
    type(fgsl_integration_workspace) :: integration_workspace

    array_size = size(x_array)
    absolute_error = 0.0d0; relative_error = 1.0d-7
    spline_data%spline = fgsl_spline_alloc(fgsl_interp_cspline, array_size)
    spline_status = fgsl_spline_init(spline_data%spline, x_array, y_array)
    spline_c_ptr = c_loc(spline_data)
    integrable_f_function = fgsl_function_init(integrable_c_function, spline_c_ptr)
    integration_workspace = fgsl_integration_workspace_alloc(max_number_intervals)
    integration_status = fgsl_integration_qags(integrable_f_function, a, b, absolute_error, relative_error, & 
                                               max_number_intervals, integration_workspace, & 
                                               GetSplineIntegral, integration_error)
    call fgsl_function_free(integrable_f_function)
    call fgsl_integration_workspace_free(integration_workspace)
    call fgsl_spline_free(spline_data%spline)
    call fgsl_interp_accel_free(spline_data%acceleration)
  end function GetSplineIntegral

end module
